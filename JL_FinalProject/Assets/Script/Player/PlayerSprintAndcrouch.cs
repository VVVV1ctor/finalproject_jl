﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprintAndcrouch : MonoBehaviour
{
    private Playermovement playerMovement;

    public float sprint_Speed = 10f;
    public float move_Speed = 5f;
    public float crouch_Speed = 2f;

    private Transform look_Root;
    private float stand_Height = 1.6f;
    private float crouch_Height = 1f;

    private bool is_Crouching;



    private float sprint_Volume = 1f;
    private float crouch_Volume = 0.1f;
    private float walk_Volume_Min = 0.2f, walk_Volume_Max = 0.6f;

    private float walk_Step_Distance = 0.4f;
    private float sprint_Step_Distance = 0.25f;
    private float crouch_Step_Distance = 0.5f;



    private float sprint_Value = 100f;
    public float sprint_Treshold = 10f;

    void Awake()
    {

        playerMovement = GetComponent<Playermovement>();

        look_Root = transform.GetChild(0);
    }
        

  
    void Update()
    {
        Sprint();
        Crouch();
    }

    void Sprint()
    {

        
        if (sprint_Value > 0f)
        {

            if (Input.GetKeyDown(KeyCode.LeftShift) && !is_Crouching)
            {

                playerMovement.speed = sprint_Speed;


            }

        }

        if (Input.GetKeyUp(KeyCode.LeftShift) && !is_Crouching)
        {

            playerMovement.speed = move_Speed;

       
        }

        if (Input.GetKey(KeyCode.LeftShift) && !is_Crouching)
        {

            sprint_Value -= sprint_Treshold * Time.deltaTime;

            if (sprint_Value <= 0f)
            {

                sprint_Value = 0f;



            }

            

        }
        else
        {

            if (sprint_Value != 100f)
            {

                sprint_Value += (sprint_Treshold / 2f) * Time.deltaTime;

              

                if (sprint_Value > 100f)
                {
                    sprint_Value = 100f;
                }

            }

        }


    } // sprint

    void Crouch()
    {

        if (Input.GetKeyDown(KeyCode.C))
        {

           
            if (is_Crouching)
            {

                look_Root.localPosition = new Vector3(0f, stand_Height, 0f);
                playerMovement.speed = move_Speed;

             

                is_Crouching = false;

            }
            else
            {
                

                look_Root.localPosition = new Vector3(0f, crouch_Height, 0f);
                playerMovement.speed = crouch_Speed;


                is_Crouching = true;

            }

        } // if we press c


    } // crouch
}
